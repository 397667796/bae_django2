#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Create your views here.

from books.models import Publisher, Book
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context


def book(request):
    p1 = Publisher(name='Apress', address='2855 Telegraph Avenue',
         city='Berkeley', state_province='CA', country='U.S.A.',
         website='http://www.apress.com/')
    p1.save()
    return HttpResponse(Publisher.objects.filter(name__contains='press'))

def search_form(request):
    title = 'Search_form'
    t = get_template('search_form.html')
    html = t.render(Context({'title': title}))
    return HttpResponse(html)

def search(request):
    title = 'Search'
    t = get_template('search.html')
    books = None
    q = ''
    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']
        books = Book.objects.filter(title__icontains=q)
    html = t.render(Context({'title': title, 'books': books, 'search_item': q}))
    return HttpResponse(html)
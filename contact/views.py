#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Create your views here.
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context

def contact(request):
    t = get_template('contact.html')
    html = t.render(Context())
    return HttpResponse(html)
# BAE Django示例

## Overview
将作为主页等简单动态网站。   
之前的bae\_django提交历史有点乱，不再维护，改为维护这个bae\_django2.

## DirectoryTree
    |-- bae_django2 (Django project配置目录)
    |-- hello (Django的一个app hello目录)
    |-- static (Django静态文件目录)
    ...

具体参考BAE简陋的[文档](http://developer.baidu.com/wiki/index.php?title=docs/cplat/rt/python#.E5.87.BA.E9.94.99.E5.A4.84.E7.90.86.E3.80.81.E6.97.A5.E5.BF.97.E3.80.81.E6.A0.87.E5.87.86.E8.BE.93.E5.87.BA).

## Copyright and license

Copyright 2013 Honghe Wu.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

  [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

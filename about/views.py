#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Create your views here.
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context

def about(request):
    t = get_template('about.html')
    html = t.render(Context())
    return HttpResponse(html)

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'bae_django2.views.home', name='home'),
    # url(r'^bae_django2/', include('bae_django2.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    # user:admin pswd:654321
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'hello.views.hello', name='hello'),
    url(r'^about/$', 'about.views.about', name='about'),
    url(r'^book/$', 'books.views.book', name='book'),
    url(r'^contact/$', 'contact.views.contact', name='contact'),
    url(r'^search-form/$', 'books.views.search_form', name='search_form'),
    url(r'^search/$', 'books.views.search', name='search'),
)
urlpatterns += staticfiles_urlpatterns()
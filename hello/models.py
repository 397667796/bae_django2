#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
import datetime


class Message(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField(max_length=40)
    message = models.CharField(max_length=140)
    datetime = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return u'%s %s %s' % (self.name, self.email, self.datetime)

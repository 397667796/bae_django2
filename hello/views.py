#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Create your views here.
import datetime
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template

def hello(resquest):
    now = datetime.datetime.now()
    greeting = u'说点什么:)'
    all_meta = resquest.META.items()
    t = get_template('hello.html')
    html = t.render(Context({'current_date': now, 'greeting': greeting, 'all_meta': all_meta}))
    return HttpResponse(html)
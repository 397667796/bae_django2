#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on 7/18/13
"""
from django.contrib import admin
from models import Message
__author__ = 'honghe'

class MessageAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'message')
    search_fields = ('name', 'email')
    list_filter = ('name', 'email')
    ordering = ('datetime',)

admin.site.register(Message, MessageAdmin)
